/*global angular*/

var app = angular.module('angular', [])

app.controller('slidesController', function($scope) {
    $scope.image = '/media/slideshow1.jpg'
    
    $scope.image1 = function($event) {
        $scope.image = '/media/slideshow1.jpg'
    }
    $scope.image2 = function($event) {
        $scope.image = '/media/slideshow2.jpg'
    }
    $scope.image3 = function($event) {
        $scope.image = '/media/slideshow3.jpg'
    }
    $scope.image4 = function($event) {
        $scope.image = '/media/slideshow4.jpg'
    }
    $scope.image5 = function($event) {
        $scope.image = '/media/slideshow5.jpg'
    }
    $scope.image6 = function($event) {
        $scope.image = '/media/slideshow6.jpg'
    }
})

app.controller('followsController', function($scope, $window) {
    $scope.twitterFollow = function($event) {
        $window.open('https://www.twitter.com/', '_blank')
    }
    $scope.instagramFollow = function($event) {
        $window.open('https://www.instagram.com/', '_blank')
    }
    $scope.submitUser = function($event) {
        $window.alert('Placeholder function for submit feature.')
    }
})